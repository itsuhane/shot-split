#include <array>
#include <string>
#include <memory>
#include <fstream>
#include <iostream>
#include <locale>
#include <codecvt>
#include <opencv2/opencv.hpp>
//#define NOMINMAX
//#include <Windows.h>

cv::Size get_video_size(const cv::VideoCapture &video) {
    return cv::Size((int)video.get(cv::CAP_PROP_FRAME_WIDTH), (int)video.get(cv::CAP_PROP_FRAME_HEIGHT));
}

#if 0
size_t find_split_frame(const std::string &filename) {
    cv::VideoCapture video(filename);
    if (!video.isOpened()) return 0;
    cv::Mat frame, oldframe;
    size_t split_fid = 0;
    size_t fid = 0;
    double max_score = 0;
    while (true) {
        video >> frame;
        if (frame.empty()) break;
        
        if (!oldframe.empty()) {
            cv::Mat framediff;
            cv::absdiff(frame, oldframe, framediff);
            cv::Scalar sum = cv::sum(framediff);
            double score = 0;
            for (int i = 0; i < sum.channels; ++i) {
                score += sum[i];
            }
            if (score > max_score) {
                split_fid = fid;
                max_score = score;
            }
        }

        oldframe = frame.clone();
        fid++;
    }
    return split_fid;
}
#endif

void save_video_segment(const std::string &inputfile, const std::string &outputfile, size_t start, size_t end) {
    cv::VideoCapture video(inputfile);
    if (!video.isOpened()) return;
    double fps = video.get(cv::CAP_PROP_FPS);
    video.set(cv::CAP_PROP_POS_FRAMES, (double)start);

    cv::VideoWriter recorder;
    recorder.open(outputfile, 0x20, fps, get_video_size(video));

    cv::Mat frame;
    size_t fid = start;
    while (true) {
        video >> frame;
        if (frame.empty()) break;
        recorder << frame;
        fid++;
        if (fid == end) break;
    }
}

std::string get_pbf_path(const std::string &mp4path) {
    return mp4path.substr(0, mp4path.size() - 3) + "pbf";
}

// To Aayush: PBF is the PotPlayer Bookmark File
// It reads a pbf file, get all bookmarks in msecs.
std::vector<size_t> load_pbf_bookmarks(const std::string &pbfpath) {
    std::vector<size_t> result;

    std::wifstream pbf(pbfpath);
    pbf.imbue(std::locale(pbf.getloc(), new std::codecvt_utf16<wchar_t, 0x10ffff, std::consume_header>()));

    std::wstring line;
    while (std::getline(pbf, line)) {
        line.erase(line.find_last_not_of(L" \n\r\t") + 1);
        if (line.length() > 0 && line != L"[Bookmark]") {
            size_t loc = line.find(L'=');
            if (loc != std::wstring::npos && loc+1<line.length()) {
                size_t msec = stoi(line.substr(loc + 1));
                result.push_back(msec);
            }
        }
    }
    return result;
}

size_t find_split_frame(cv::VideoCapture &video, size_t start, size_t end) {
    cv::Mat frame, oldframe;
    video.set(cv::CAP_PROP_POS_FRAMES, double(start));
    size_t split_fid = start;
    size_t fid = start;
    double max_score = 0;
    while (true) {
        video >> frame;
        if (frame.empty()) break;

        if (!oldframe.empty()) {
            cv::Mat framediff;
            cv::absdiff(frame, oldframe, framediff);
            cv::Scalar sum = cv::sum(framediff);
            double score = 0;
            for (int i = 0; i < sum.channels; ++i) {
                score += sum[i];
            }
            if (score > max_score) {
                split_fid = fid;
                max_score = score;
            }
        }

        oldframe = frame.clone();
        fid++;
        if (fid >= end) break;
    }

    return split_fid;
}

std::vector<size_t> find_all_split_frame(const std::string &mp4path) {
    cv::VideoCapture video(mp4path);
    if (!video.isOpened()) {
        return{};
    }

    // bookmarks in msec
    std::vector<size_t> bookmarks = load_pbf_bookmarks(get_pbf_path(mp4path));

    // convert to bookmarks in frame
    double fps = video.get(cv::CAP_PROP_FPS);
    for (auto & b : bookmarks) {
        b = size_t(b*fps/1000.0);
    }

    std::sort(bookmarks.begin(), bookmarks.end());
    bookmarks.resize(std::distance(bookmarks.begin(), std::unique(bookmarks.begin(), bookmarks.end())));

    for (auto &s : bookmarks) {
        std::cout << '\t' << s;
    }
    std::cout << std::endl;

    std::vector<std::pair<size_t, size_t>> ranges;
    for (auto &s : bookmarks) {
        ranges.emplace_back(size_t(std::max((double)s-fps*2, 0.0)), size_t(s+fps*2));
    }

    std::vector<size_t> splits;
    for (auto &r : ranges) {
        splits.push_back(find_split_frame(video, r.first, r.second));
    }

    splits.push_back(0);
    splits.push_back((size_t)video.get(cv::CAP_PROP_FRAME_COUNT));

    std::sort(splits.begin(), splits.end());
    splits.resize(std::distance(splits.begin(), std::unique(splits.begin(), splits.end())));

    for (auto &s : splits) {
        std::cout << '\t' << s;
    }
    std::cout << std::endl;

    return splits;
}

int main(int argc, char *argv[]) {
    if (argc != 2) {
        printf("Usage: %s <mp4>", argv[0]);
    }

    std::vector<size_t> splits = find_all_split_frame(argv[1]);

    for (size_t i = 1; i < splits.size(); ++i) {
        char fn[500];
        sprintf_s(fn, "%06zd.mp4", splits[i - 1]);
        save_video_segment(argv[1], fn, splits[i - 1], splits[i]);
    }

    return 0;
}
